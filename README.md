**Updates:
**
Representative notebooks have been pushed to the repo. For access to individual notebooks used for computations, refer to flashmob03.

**Samples: 2
**

* *Banned-subreddits*: contains all posts from r/fatpeoplehate and r/coontown made during 01-2015 to 06-2015(time of ban); size = 1.5 million posts.
* *Control-subreddits*: contains a random sample of 1.5 million posts (size equal to treatment group) obtained from rest of Reddit during 01-2015 to 06-2015;Obtained using Big Query table.


**Method:
**

* Tokenization done with nltk.tokenize RegexpTokenizer. Regular expression used for tokenizing:
        tokenizer = RegexpTokenizer(r'\w+')
* LIWC categories used: *anger,death,sexual,negative_affect,anxiety,sadness,swear*.


**Results:
**

* <sample-name>-LIWC.csv: contains LIWC values computed for certain categories along with total_comments in the sample.
* <sample-name>-text.csv: contains the text present in posts from each sample.
* <sample-name>-vocabulary.csv: contains ALL the terms present in the sample's vocabulary; Vocabulary size for both samples happened to be < 1 million (control: ~ 700K, banned: ~ 350K).

Links to Data:


https://bigquery.cloud.google.com/jobs/api-project-687903306508  
https://console.cloud.google.com/storage/browser/reddit-control/