psm = read.csv("/nethome/asrinivasan45/psm_fph_data_all_features.csv", header=TRUE)

library(MatchIt)
library(dplyr)
library(ggplot2)

psm = psm[,c("author", "preban", "treatment", "comment_karma", "link_karma", "age")]

m = matchit(treatment ~ preban + age + comment_karma + link_karma, method = "nearest", data = psm, ratio=1 )

final = match.data(m)

write.csv (final, file="fph_matched_all_data_all_features.csv")

write.csv(m$match.matrix, file="fph_matched_users_all_features.csv")
