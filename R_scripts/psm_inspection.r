library(MatchIt)
library(dplyr)
library(ggplot2)

dta_m2 = read.csv('/Users/anirudhsrinivasan/Desktop/MP2/PSM_RESULTS/ct_mdm_matched_preban_age_all_data.csv')
dta_m2 = dta_m2[,c('author', 'preban', 'age' ,'treatment', 'weights')]

wilcox.test(dta_m2[,('preban')] ~ dta_m2$treatment)
wilcox.test(dta_m2[,('comment_karma')] ~ dta_m2$treatment)
wilcox.test(dta_m2[,('age')] ~ dta_m2$treatment)

fn_bal <- function(dta, variable) {
  dta$variable <- dta[, variable]
  dta$treatment <- as.factor(dta$treatment)
  support <- c(min(dta$variable), max(dta$variable))
  ggplot(dta, aes(x = distance, y = variable, color = treatment, group=1)) +
    geom_point(alpha = 0.2, size = 1.3) +
    geom_smooth(method = "loess", se = F) +
    xlab("Propensity score") +
    ylab(variable) +
    theme_bw() +
    ylim(support)
}

library(gridExtra)
grid.arrange(
  fn_bal(dta_m2, "preban"),
  fn_bal(dta_m2, "comment_karma"),
  fn_bal(dta_m2, "age") + theme(legend.position = "none"),
  nrow = 3, widths = c(1, 0.8)
)

library(gridExtra)
grid.arrange(fn_bal(dta_m2, "age"),
             nrow = 3, widths = c(1, 0.8))


