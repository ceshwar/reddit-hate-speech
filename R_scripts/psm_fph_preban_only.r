psm = read.csv("/nethome/asrinivasan45/psm_fph_data_preban_only.csv", header=TRUE)

library(MatchIt)
library(dplyr)
library(ggplot2)

psm = psm[,c("author", "preban", "treatment")]

m = matchit(treatment ~ preban, method = "nearest", data = psm, ratio=1 )

final = match.data(m)

write.csv (final, file="fph_matched_all_data_preban_only.csv")

write.csv(m$match.matrix, file="fph_matched_users_preban_only.csv")
